/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CHANGEFILEEVALUATOR_H
#define CHANGEFILEEVALUATOR_H

#include <iostream>
#include <sys/stat.h>
#include "fileevaluator.h"
#include "filetypes.h"
#include "epidb.h"
#include "fileops.h"

class ChangeFileEvaluator : public FileEvaluator {
public:
    ChangeFileEvaluator(EpiDb& db) : db(db) {}
    
    void eval_path(const std::string& file_path, const struct stat* le_stat);
    void close_dir();
    void open_dir(const std::string& dir_path);
    
private:
    EpiDb& db;
    std::string loaded_parent_path;
    filebrief_map_t loaded_parent;
};

#endif // CHANGEFILEEVALUATOR_H
