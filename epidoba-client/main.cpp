#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/stat.h>
#include "epiconf.h"
#include "epidb.h"
#include "fileops.h"
#include "changefileevaluator.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "ERROR: Program takes 1 argument, a configuration file path" << std::endl;
        return 1;
    }
    
    std::string conf_path(argv[1]);
    EpiConf* epiconf = new EpiConf(conf_path);
    int conf_parse_result = epiconf->parse_config();
    
    if (conf_parse_result > 0) {
        return conf_parse_result;
    }
    
    std::cout << "Database Path: " << epiconf->dbpath << std::endl;
    std::cout << "Epidoba Server: " << epiconf->server_hostname << std::endl;
    std::cout << "Epidoba Server Port: " << epiconf->server_port << std::endl;
    std::cout << "Extent Size: " << epiconf->extent_size_hrs << std::endl;
    
    EpiDb* db = new EpiDb(epiconf->dbpath);
    if (!db->open_success()) { std::cerr << "Error opening database " << epiconf->dbpath << " : " << db->db_error_msg() << std::endl; }
    
    std::vector<BackupSource>::iterator sit = epiconf->sources.begin();
    for (;sit != epiconf->sources.end(); ++sit) {
        BackupSource& source = *sit;
        std::cout << "Source Path: " << source.spath << std::endl;
        
        std::vector<std::string>::iterator eit = source.excludes.begin();
        for (; eit != source.excludes.end(); ++eit) {
            std::cout << "    Exclude: " << *eit << std::endl;
        }
        
        std::cout << "Walking path: " << source.spath << std::endl;
        ChangeFileEvaluator cfe(*db);
        
        try {
            walk_tree(source.spath, cfe);
        } catch (DbException dbe) {
            std::cerr << "ERROR, DB: " << dbe.msg << std::endl;
        }
    }
    
//     std::cout << "Hashing a nice file..." << std::endl;
//     unsigned char hash_buf[HASH_LENGTH];
//     int hash_result = compute_file_hash("/usr/bin/Xorg", hash_buf);
//     
//     if (hash_result == HASH_OK) {
//         std::cout << "Fetching filebriefs for dir /usr/bin from database..." << std::endl;
//         
//         struct stat stat_buf;
//         stat("/usr/bin/Xorg", &stat_buf);
//         FileBrief filebrief(stat_buf.st_size, stat_buf.st_mtime, hash_buf);
//         
//         filebrief_map_t briefs;
//         db->get_filebriefs_for_parent("/usr/bin", briefs);
//         filebrief_map_t::iterator fbit = briefs.find("Xorg");
//         
//         if (fbit == briefs.end()) {
//             std::cout << "No filebrief for Xorg found in database, adding..." << std::endl;
//             
//             try {
//                 db->add_filebrief("/usr/bin", "Xorg", filebrief);
//             } catch (DbException dbe) {
//                 std::cerr << "ERROR, DB: Writing filebrief record: " << dbe.what() << std::endl;
//             }
//         } else {
//             if (filebrief.compare(fbit->second)) {
//                 std::cout << "File on disk matches database record." << std::endl;
//             } else {
//                 std::cout << "File on disk does NOT match database record." << std::endl;
//             }
//         }
//         
//         
//         std::stringstream hash_ss;
//         for (int i = 0; i < HASH_LENGTH; i++) {
//             hash_ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash_buf[i];
//         }
//         std::cout << hash_ss.str() << std::endl;
//     }
    
    delete db;
    delete epiconf;
    
    return 0;
}
