#include "fileops.h"

int compute_file_hash(const std::string& file_path, unsigned char* hash_buf) {
    int result = HASH_OK;
    
    SHA256_CTX hash_ctx;
    SHA256_Init(&hash_ctx);
    char inbuf[COMPUTE_HASH_FILE_BUF_SIZE];
    
    std::ifstream file_st(file_path, std::ifstream::in | std::ifstream::binary);
    while (1) {
        file_st.read(inbuf, COMPUTE_HASH_FILE_BUF_SIZE);
        
        if (!file_st.eof() && file_st.fail()) {
            result = HASH_IO_ERROR;
            break;
        }
        
        SHA256_Update(&hash_ctx, inbuf, file_st.gcount());
        
        if (file_st.eof()) { break; }
    }
    
    file_st.close();
    if (result == HASH_OK) {
        SHA256_Final(hash_buf, &hash_ctx);
    }
    
    return result;
}

void walk_tree(const std::string& root_path, FileEvaluator& evaluator) {
    std::stack<std::string> dirstack;
    dirstack.emplace(root_path);
    
    DIR* dir;
    dirent* dent;
    struct stat ch_stat;
    std::string dir_path;
    std::string ch_path;
    
    do {
        dir_path.assign(dirstack.top());
        dirstack.pop();
        
        dir = opendir(dir_path.c_str());
        if (!dir) { continue; }
        
        while ((dent = readdir(dir)) != nullptr) {
            if (dent->d_name[0] == '.' && dent->d_name[1] == '\0') { continue; }
            if (dent->d_name[0] == '.' && dent->d_name[1] == '.' && dent->d_name[2] == '\0') { continue; }
            
            ch_path.assign(dir_path);
            ch_path.append("/");
            // NOTE - (dirent)dent->d_reclen was ALWAYS 32 when this part was first developed
            ch_path.append(dent->d_name);
            
            if (lstat(ch_path.c_str(), &ch_stat) == 0) {
                if (S_ISDIR(ch_stat.st_mode)) {
                    dirstack.emplace(ch_path);
                    evaluator.eval_path(ch_path, &ch_stat);
                } else if (S_ISREG(ch_stat.st_mode)) {
                    evaluator.eval_path(ch_path, &ch_stat);
                }
            }
        }
        
        closedir(dir);
    } while (dirstack.size() > 0);
}