#ifndef FILETYPES_H
#define FILETYPES_H

#include <string>
#include <cstring>
#include <unordered_map>
#include <openssl/sha.h>

#define HASH_LENGTH SHA256_DIGEST_LENGTH

class FileBrief {
public:
    FileBrief(long long f_size, long long f_mtime, const void* f_sha256);
    
    bool compare(const FileBrief& other);
    bool compare_mod_size(long long other_size, long long other_mtime);
    
    long long int f_size;
    long long int f_mtime;
    unsigned char f_sha256[32];
};

typedef std::unordered_map< std::string, FileBrief > filebrief_map_t;

#endif