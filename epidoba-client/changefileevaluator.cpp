/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "changefileevaluator.h"

void ChangeFileEvaluator::eval_path(const std::string& file_path, const struct stat* le_stat) {
    // Split file name off path
    int last_slash_idx = 0;
    std::string f_parent;
    std::string f_name;
    unsigned char hash_buf[HASH_LENGTH];
    
    for (int i = file_path.length(); last_slash_idx == 0 && i > 0; i--) {
        if (file_path[i] == '/') { last_slash_idx = i; }
    }
    
    if (last_slash_idx == 0) { std::cerr << "ERROR: Path has no last slash: " << file_path << std::endl; return; }
    
    f_parent.assign(file_path, 0, last_slash_idx);
    f_name.assign(file_path, last_slash_idx + 1, file_path.length() - last_slash_idx - 1);
    
    if (loaded_parent_path.compare(f_parent) != 0) {
        loaded_parent.clear();
        db.get_filebriefs_for_parent(f_parent, loaded_parent);
        loaded_parent_path.assign(f_parent);
    }
    
    filebrief_map_t::iterator fbi;
    fbi = loaded_parent.find(f_name);
    
    if (fbi == loaded_parent.end()) {
        std::cout << "New File: " << file_path << std::endl;
        compute_file_hash(file_path, hash_buf);
        db.add_filebrief(f_parent, f_name, FileBrief(le_stat->st_size, le_stat->st_mtime, hash_buf));
    } else {
        if (!fbi->second.compare_mod_size(le_stat->st_size, le_stat->st_mtime)) {
            std::cout << "File Changed: " << file_path << std::endl;
            compute_file_hash(file_path, hash_buf);
            db.update_filebrief(f_parent, f_name, FileBrief(le_stat->st_size, le_stat->st_mtime, hash_buf));
        }
    }
}

void ChangeFileEvaluator::open_dir(const std::string& dir_path) {

}

void ChangeFileEvaluator::close_dir() {

}
