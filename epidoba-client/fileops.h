#ifndef FILEOPS_H
#define FILEOPS_H

#include <fstream>
#include <stack>
#include <openssl/sha.h>
#include <dirent.h>
#include <sys/stat.h>
#include "epidb.h"
#include "filetypes.h"
#include "fileevaluator.h"

#define COMPUTE_HASH_FILE_BUF_SIZE 65536
#define HASH_OK 0
#define HASH_IO_ERROR 1

int compute_file_hash(const std::string& file_path, unsigned char* hash_buf);
void walk_tree(const std::string& root_path, FileEvaluator& evaluator);

#endif