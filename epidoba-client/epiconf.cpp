/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "epiconf.h"

EpiConf::EpiConf(const std::string& conf_path) : _conf_path(conf_path) {
    server_port = DEF_SERVER_PORT;
    extent_size_bytes = DEF_EXTENT_SIZE_BYTES;
}

int EpiConf::parse_config() {
    std::vector<std::string> stmt_buf;
    char tok_buf[TOK_BUF_SIZE];
    
    char c = '\0';
    bool gobbling = true;
    bool gob_to_nl = false;
    int tok_pos = 0;
    
    int result = 0;
    
    std::ifstream conf_st(_conf_path, std::ifstream::in);
    while (1) {
        // Bound check token buffer
        if (tok_pos > TOK_BUF_SIZE - 1) {
            std::cerr << "ERROR, CONFIG: Token buffer overflow" << std::endl;
            result = 1;
            break;
        }
        
        // Get a character!
        conf_st.get(c);
        
        // Handle stream error
        if (!conf_st.eof() && conf_st.fail()) {
            char *errstr = strerror(errno);
            std::cerr << "Failed to read " << _conf_path << ": " << errstr << std::endl;
            result = 1;
            break;
        }
        
        // Parse character if not EOF
        if (!conf_st.eof()) {
            if (gob_to_nl) {
                if (c != '\n') { continue; }
                else { gob_to_nl = false; }
            }
            
            if (gobbling) {
                if (c == '#') { gob_to_nl = true; }
                else if (c != ' ' && c != '\t' && c != '\r' && c != '\n') { gobbling = false; }
            }
            
            if (!gobbling) {
                // End of token
                if (c == ' ' || c == '\t' || c == '\r' || c == '\n' || c == ';') {
                    tok_buf[tok_pos] = '\0';
                    std::string tok = std::string(tok_buf);
                    stmt_buf.push_back(tok);
                    tok_pos = 0;
                    
                    gobbling = true;
                    
                    // End of token and statement
                    if (c == ';') {
                        std::vector<std::string>::iterator sit = stmt_buf.begin();
                        std::string& directive(*sit);
                        
                        // database directive - indicating the path to the local backup state database
                        if (directive.compare("database") == 0) {
                            if (stmt_buf.size() != 2) {
                                std::cerr << "ERROR, CONFIG: database directive accepts only one parameter" << std::endl;
                                result = 1; break;
                            }
                            
                            ++sit;
                            dbpath = std::string(*sit);
                        }
                        // source directive - indicating a backup source
                        else if (directive.compare("source") == 0) {
                            if (stmt_buf.size() < 2) {
                                std::cerr << "ERROR, CONFIG: source directive encountered with too few parameters" << std::endl;
                                result = 1; break;
                            }
                            
                            ++sit;
                            std::string& spath(*sit);
                            BackupSource source(spath);
                            
                            ++sit;
                            while (sit != stmt_buf.end()) {
                                std::string sparam = std::string(*sit);
                                if (sparam[0] == '-') {
                                    std::string source_exclude(sparam.substr(1, sparam.length() - 1));
                                    source.excludes.push_back(source_exclude);
                                }
                                
                                ++sit;
                            }
                            
                            sources.push_back(source);
                        }
                        // server directive - indicating the Epidoba server to which backups will be pushed
                        else if (directive.compare("server") == 0) {
                            std::string port_str;
                            short colon_count = 0;
                            
                            if (stmt_buf.size() < 2) {
                                std::cerr << "ERROR, CONFIG: server directive requires one parameter, hostname:port" << std::endl;
                                result = 1; break;
                            }
                            
                            ++sit;
                            std::string& host_port(*sit);
                            
                            for (int i = 0; i < host_port.length(); i++) {
                                if (host_port[i] == ':') {
                                    if (colon_count > 0) {
                                        std::cerr << "ERROR, CONFIG: More than one colon in hostname:port combination for server directive" << std::endl;
                                        result = 1; break;
                                    }
                                    
                                    colon_count += 1;
                                } else {
                                    if (colon_count < 1) {
                                        server_hostname.push_back(host_port[i]);
                                    } else {
                                        port_str.push_back(host_port[i]);
                                    }
                                }
                            }
                            
                            if (colon_count > 0) {
                                try {
                                    server_port = std::stoi(port_str);
                                } catch (std::exception e) {
                                    std::cerr << "ERROR, CONFIG: Invalid integer port value: " << port_str << std::endl;
                                    result = 1; break;
                                }
                            }
                        }
                        // extent_size_bytes - indicating the maximum size of a backup data extent
                        else if (directive.compare("extent_size_bytes") == 0) {
                            if (stmt_buf.size() < 2) {
                                std::cerr << "ERROR, CONFIG: extent_size_bytes directive requires one parameter, and integer" << std::endl;
                                result = 1; break;
                            }
                            
                            ++sit;
                            try {
                                extent_size_bytes = std::stol(*sit);
                            } catch (std::exception e) {
                                std::cerr << "ERROR, CONFIG: Invalid integer extent_size_bytes value: " << *sit << std::endl;
                                result = 1; break;
                            }
                        } else {
                            std::cerr << "WARNING, CONFIG: Unknown directive: " << directive << std::endl;
                        }
                        
                        stmt_buf.clear();
                    }
                } else {
                    tok_buf[tok_pos] = c;
                    tok_pos += 1;
                }
            }
        } else { break; }
    }
    
    conf_st.close();
    
    if (dbpath.length() == 0) {
        std::cerr << "ERROR, CONFIG: database directive not specified in config file" << std::endl;
        result = 1;
    }
    
    if (server_hostname.length() == 0) {
        std::cerr << "ERROR, CONFIG: server directive not specified in config file" << std::endl;
        result = 1;
    }
    
    // Prepare human readable strings for certain configuration parameters
    if (extent_size_bytes < 1024) {
        extent_size_hrs += std::to_string(extent_size_bytes) + "B";
    } else if (extent_size_bytes < 1048576) {
        extent_size_hrs += std::to_string(extent_size_bytes / 1024) + "KB";
    } else {
        extent_size_hrs += std::to_string(extent_size_bytes / 1024 / 1024) + "MB";
    }
    
    return result;
}