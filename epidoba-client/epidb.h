/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef EPIDB_H
#define EPIDB_H

#include <exception>
#include <iostream>
#include <string>
#include <sqlite3.h>
#include "filetypes.h"

class DbException : std::exception {
public:
    const char* msg;
    DbException(const char* msg);
    DbException(int sqlite_error);
    virtual const char* what() const noexcept;
};

class EpiDb {
public:
    EpiDb(const std::string& dbpath);
    ~EpiDb();
    
    bool open_success();
    const std::string& db_error_msg();
    
    void add_filebrief(const std::string& f_parent, const std::string& f_name, const FileBrief& filebrief);
    void update_filebrief(const std::string& f_parent, const std::string& f_name, const FileBrief& filebrief);
    void get_filebriefs_for_parent(const std::string& f_parent, filebrief_map_t& filebrief_map);
    
private:
    bool _open_success;
    std::string _db_error_msg;
    std::string dbpath;
    sqlite3* dbh;
};

#endif