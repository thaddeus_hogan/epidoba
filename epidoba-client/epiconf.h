/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef EPICONF_H
#define EPICONF_H

#define TOK_BUF_SIZE 2048 // 2KB token buffer
#define DEF_SERVER_PORT 1491
#define DEF_EXTENT_SIZE_BYTES 134217728 // 128 MB

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <errno.h>
#include <string.h>

typedef std::vector<std::string> exclude_vec_t;

class BackupSource {
public:
    std::string spath;
    exclude_vec_t excludes;
    BackupSource(std::string spath) : spath(spath), excludes() {}
};

typedef std::vector<BackupSource> backup_source_vec_t;

class EpiConf {
public:
    std::string dbpath;
    backup_source_vec_t sources;
    std::string server_hostname;
    uint32_t server_port;
    uint64_t extent_size_bytes;
    std::string extent_size_hrs; // Human readable string
    EpiConf(const std::string& conf_path);
    int parse_config();

private:
    std::string _conf_path;
};

#endif // EPICONF_H
