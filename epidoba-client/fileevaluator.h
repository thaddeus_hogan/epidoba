#ifndef FILEEVALUATOR_H
#define FILEEVALUATOR_H

#include <string>
#include <sys/stat.h>

class FileEvaluator {
public:
    FileEvaluator() {}
    
    // Takes a stat pointer optionally because whatever function is walking the FS probably already
    // stated the file
    virtual void eval_path(const std::string& file_path, const struct stat* le_stat) = 0;
    
    virtual void open_dir(const std::string& dir_path) = 0;
    virtual void close_dir() = 0;
};

#endif