/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "epidb.h"

DbException::DbException(const char* msg) {
    this->msg = msg;
}

DbException::DbException(int sqlite_error) {
    this->msg = sqlite3_errstr(sqlite_error);
}

const char* DbException::what() const noexcept {
    return msg;
}

EpiDb::EpiDb(const std::string& dbpath) : dbpath(dbpath) {
    _open_success = false;
    
    int dbopen_result = sqlite3_open_v2(dbpath.c_str(), &dbh, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, nullptr);
    if (dbopen_result == SQLITE_OK) {
        _open_success = true;
    } else {
        _db_error_msg.assign(sqlite3_errstr(dbopen_result));
    }
}

EpiDb::~EpiDb() {
    if (dbh != nullptr) {
        sqlite3_close_v2(dbh);
        dbh = nullptr;
    }
}

bool EpiDb::open_success() {
    return _open_success;
}

const std::string& EpiDb::db_error_msg() {
    return _db_error_msg;
}

void EpiDb::add_filebrief(const std::string& f_parent, const std::string& f_name, const FileBrief& filebrief) {
    int dbres;
    sqlite3_stmt* stmt;
    const char* sql = "INSERT INTO FILES (F_PARENT, F_NAME, F_SIZE, F_MTIME, F_SHA256) VALUES (?, ?, ?, ?, ?)";
    
    dbres = sqlite3_prepare_v2(dbh, sql, -1, &stmt, nullptr);
    if (dbres != SQLITE_OK) { sqlite3_finalize(stmt); throw DbException(dbres); }
    
    // FIXME - Check for errors after binds properly, but find a way to make it clean
    sqlite3_bind_text(stmt, 1, f_parent.c_str(), f_parent.length() * sizeof(char), SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, f_name.c_str(), f_name.length() * sizeof(char), SQLITE_STATIC);
    sqlite3_bind_int64(stmt, 3, filebrief.f_size);
    sqlite3_bind_int64(stmt, 4, filebrief.f_mtime);
    sqlite3_bind_blob(stmt, 5, filebrief.f_sha256, HASH_LENGTH, SQLITE_STATIC);
    
    dbres = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    
    if (dbres != SQLITE_DONE) { throw DbException(dbres); }
}

void EpiDb::update_filebrief(const std::string& f_parent, const std::string& f_name, const FileBrief& filebrief) {
    int dbres;
    sqlite3_stmt* stmt;
    const char* sql = "UPDATE FILES SET F_SIZE = ?, F_MTIME = ?, F_SHA256 = ? WHERE F_PARENT = ? AND F_NAME = ?";
    
    dbres = sqlite3_prepare_v2(dbh, sql, -1, &stmt, nullptr);
    if (dbres != SQLITE_OK) { sqlite3_finalize(stmt); throw DbException(dbres); }
    
    // FIXME - Check for errors after binds properly, but find a way to make it clean
    sqlite3_bind_int64(stmt, 1, filebrief.f_size);
    sqlite3_bind_int64(stmt, 2, filebrief.f_mtime);
    sqlite3_bind_blob(stmt, 3, filebrief.f_sha256, HASH_LENGTH, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 4, f_parent.c_str(), f_parent.length() * sizeof(char), SQLITE_STATIC);
    sqlite3_bind_text(stmt, 5, f_name.c_str(), f_name.length() * sizeof(char), SQLITE_STATIC);
    
    dbres = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    
    if (dbres != SQLITE_DONE) { throw DbException(dbres); }
}

void EpiDb::get_filebriefs_for_parent(const std::string& f_parent, filebrief_map_t& filebrief_map) {
    int dbres;
    sqlite3_stmt* stmt;
    const char* sql = "SELECT F_NAME, F_SIZE, F_MTIME, F_SHA256 FROM FILES WHERE F_PARENT = ?";
    
    dbres = sqlite3_prepare_v2(dbh, sql, -1, &stmt, nullptr);
    if (dbres != SQLITE_OK) { sqlite3_finalize(stmt); throw DbException(dbres); }
    
    dbres = sqlite3_bind_text(stmt, 1, f_parent.c_str(), f_parent.length() * sizeof(char), SQLITE_STATIC);
    if (dbres != SQLITE_OK) { sqlite3_finalize(stmt); throw DbException(dbres); }
    
    dbres = sqlite3_step(stmt);
    while (dbres == SQLITE_ROW) {
        // FIXME - Check here to see that database returned a sha256 colkumn value that is exactly 32 bytes
        std::string f_name((char*)sqlite3_column_text(stmt, 0));
        long long f_size = sqlite3_column_int64(stmt, 1);
        long long f_mtime = sqlite3_column_int64(stmt, 2);
        const void* f_sha256 = sqlite3_column_blob(stmt, 3);
        
        filebrief_map.emplace(std::piecewise_construct, std::make_tuple(f_name), std::make_tuple(f_size, f_mtime, f_sha256));
        
        dbres = sqlite3_step(stmt);
    }
    
    sqlite3_finalize(stmt);
    
    if (dbres != SQLITE_DONE) { throw DbException(dbres); }
}