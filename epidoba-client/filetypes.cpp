#include "filetypes.h"

FileBrief::FileBrief(long long f_size, long long f_mtime, const void* f_sha256) {
    this->f_size = f_size;
    this->f_mtime = f_mtime;
    std::memcpy(this->f_sha256, f_sha256, HASH_LENGTH);
}

bool FileBrief::compare(const FileBrief& other) {
    if (f_size != other.f_size || f_mtime != other.f_mtime) { return false; }
    return (memcmp(f_sha256, other.f_sha256, HASH_LENGTH) == 0);
}

bool FileBrief::compare_mod_size(long long other_size, long long other_mtime) {
    return (f_size == other_size && f_mtime == other_mtime);
}